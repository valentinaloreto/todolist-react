import React from 'react';

class Counter extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      time : 0
    };
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  tick() {
    this.setState({ time: this.state.time + 1 })
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }


  render() {
    return (
      <div class="col-sm-4"> Created <br/><span class="number">{ this.state.time }</span> seconds ago</div>
    );
  }

}

export default Counter;
