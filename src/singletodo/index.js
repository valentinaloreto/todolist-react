import React from 'react';
import Counter from '../counter';

function Singletodo(props) {
  return (
    <div class="card">
      <div class="row">
        <div class="col-sm-4 text-col">
          <h5 class="card-title">{ props.todo }</h5>
        </div>
        <Counter/>
        <div class="col-sm-4 button-col">
          <button  class="btn btn-danger" onClick={ props.delete }>Delete</button>
        </div>
      </div>
      <hr/>
    </div>
  );
}

// class Singletodo extends React.Component {
//   constructor(props) { super(props); }
//   render() {
//     return (
//       <div class="card">
//         <div class="row">
//           <div class="col-sm-4 text-col">
//             <h5 class="card-title">{ this.props.todo }</h5>
//           </div>
//           <Counter/>
//           <div class="col-sm-4 button-col">
//             <button  class="btn btn-danger" onClick={  this.props.delete }>Delete</button>
//           </div>
//         </div>
//         <hr/>
//       </div>
//     );
//   }
// }

export default Singletodo;
