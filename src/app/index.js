import React from 'react';
import Singletodo from '../singletodo';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      currentTodo: ''
    };
  }

  onInputChange(e) {
    this.setState({ currentTodo: e.target.value });
  }

  onClick() {
    let todosCopy = this.state.todos.slice();
    if (this.state.currentTodo != '') {
      todosCopy.push(this.state.currentTodo);
      this.setState({ todos: todosCopy, currentTodo: '' });
    } else {
      alert('invalid input');
    }
  }

  deleteTodo(i) {
    let todosCopy = this.state.todos.slice();
    todosCopy.splice(i, 1);
    this.setState({ todos: todosCopy });
  }

  render() {
    let bulletedtodos = this.state.todos.map( (e, i) => {
      return <Singletodo todo={e} delete={ () => { this.deleteTodo(i) } }/>
    });

    return (
      <div class="form-group">
        <input class="form-control" value={this.state.currentTodo} placeholder="Enter todo" onChange={ (e) => { this.onInputChange(e) } } /><br /><br />
        <button  class="btn btn-info btn-block" onClick={ () => { this.onClick() } }>  Add</button>
        <br />
        <br />
        <h4>Current To Do's</h4>
        <hr/>
        {this.state.todos.length === 0 ? '' : <div>{ bulletedtodos }</div> }
      </div>
    );
  }

}

export default App;



// constructor(props) {
//   super(props);
//   this.state = {
//     count: 0
//   }
// }

// increment(){
//   this.setState({ count: this.state.count + 1});
// }


// render() {
//   return (
//     <div>
//       <button onClick={ () => { this.increment()} }>Increment</button>
//       {this.state.count}
//     </div>
//   );
// }
