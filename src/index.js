import React, { Component } from 'react';
import ReactDom from 'react-dom';
import App from './app';
//import '../css/styles.css';

document.addEventListener('DOMContentLoaded',() => {
  ReactDom.render(
    <div>
      <App/>
    </div>,
    document.getElementById('app')
  );
});





