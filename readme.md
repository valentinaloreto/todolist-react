Se requiere de la creacion de una aplicacion en react para la creacion de tareas que tenga y se comporte de la siguiente manera:

1. Debe incluir un formulario (un input de texto) para escribir el nombre de la tarea.

2. Debe existir un boton "añadir" que al presionarlo, limpie el input de texto y añada la tarea a un listado de tareas.

4. Cada tarea debe tener el "nombre" de la tarea, un "contador" que muestre el tiempo que lleva añadida la tarea (ej: Añadida hace XXXX seg) y un boton de "borrar"

5. Al presionar el boton borrar, debe borrarse la tarea asociada
